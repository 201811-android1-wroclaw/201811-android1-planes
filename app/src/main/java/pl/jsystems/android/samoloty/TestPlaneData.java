package pl.jsystems.android.samoloty;

import java.util.ArrayList;
import java.util.List;

public class TestPlaneData {
    public static final List<Plane> PLANES = new ArrayList<>();

    static {
        PLANES.add(new Plane(1, "Boeing 737", 5, PlaneCategory.PASSENGER));
        PLANES.add(new Plane(2, "Boeing 777", 2, PlaneCategory.PASSENGER));
        PLANES.add(new Plane(3, "Cesna 1000", 10, PlaneCategory.PERSONAL));
        PLANES.add(new Plane(4, "MIG", 20, PlaneCategory.MILITARY));
        PLANES.add(new Plane(5, "GLS Plane", 4, PlaneCategory.TRANSPORT));
        PLANES.add(new Plane(6, "F 16", 2, PlaneCategory.MILITARY));
        PLANES.add(new Plane(7, "DHL Plane", 8, PlaneCategory.PERSONAL));
        PLANES.add(new Plane(8, "JumboJet", 10, PlaneCategory.TRANSPORT));
        PLANES.add(new Plane(9, "Airbus XXL", 30, PlaneCategory.PASSENGER));
        PLANES.add(new Plane(10, "USA Navi", 1, PlaneCategory.MILITARY));

        PLANES.get(5).setEnabled(false);
        PLANES.get(7).setEnabled(false);
        PLANES.get(8).setEnabled(false);
    }

    public static List<Plane> getPlanes() {
        return PLANES;
    }

    public static void addPlane(Plane plane) {
        PLANES.add(plane);
    }
}
