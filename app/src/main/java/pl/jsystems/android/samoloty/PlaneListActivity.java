package pl.jsystems.android.samoloty;

import java.util.Collections;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

public class PlaneListActivity extends AppCompatActivity {
    private RecyclerView planeList;
    private PlaneAdapter adapter;
    private BroadcastReceiver locationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            new FetchPlanesAsyncTask(adapter).execute();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plane_list);

        planeList = findViewById(R.id.plane_list);
        // Wyświetlanie pionowo w normalnej kolejności
        planeList.setLayoutManager(new LinearLayoutManager(this));
        adapter = new PlaneAdapter(Collections.EMPTY_LIST);
        planeList.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new FetchPlanesAsyncTask(adapter).execute();

        registerReceiver(locationReceiver,
                new IntentFilter(PlaneLocationUpdateService.ACTION_LOCATION_UPDATE));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(locationReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_plane_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_add_plane) {
            Intent intent = new Intent(this, PlaneFormActivity.class);
            startActivity(intent);
            return true;
        } else if (item.getItemId() == R.id.item_update_location) {
            Intent intent = new Intent(this, PlaneLocationUpdateService.class);
            startService(intent);
            return true;
        } else if (item.getItemId() == R.id.item_show_map) {
            Intent intent = new Intent(this, MapsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
