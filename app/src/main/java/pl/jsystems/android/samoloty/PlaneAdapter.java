package pl.jsystems.android.samoloty;

import java.util.List;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PlaneAdapter
        extends RecyclerView.Adapter<PlaneAdapter.PlaneViewHolder> {
    private List<Plane> planes;

    public PlaneAdapter(List<Plane> planes) {
        this.planes = planes;
    }

    public void setPlanes(List<Plane> planes) {
        this.planes = planes;
    }

    @NonNull
    @Override
    public PlaneViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rowView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_plane, viewGroup, false);
        return new PlaneViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull PlaneViewHolder planeViewHolder, int i) {
        Plane plane = planes.get(i);

        planeViewHolder.itemView.getBackground()
                .setLevel(plane.isEnabled() ? 2 : 7);
        planeViewHolder.tvPlaneName.setText(plane.getName());
        planeViewHolder.tvPlaneCategory
                .setText(plane.getCategory() != null
                        ? plane.getCategory().name()
                        : "-");
        if (plane.getLatitude() == null) {
            planeViewHolder.tvLocation.setText("-");
        } else {
            String locationData = String.format("Lat: %.4f\nLon: %.4f\nAlt: %.4f", plane.getLatitude(), plane.getLongitude(), plane.getAltitude());
            planeViewHolder.tvLocation.setText(locationData);
        }
    }

    @Override
    public int getItemCount() {
        return planes.size();
    }

    static class PlaneViewHolder extends RecyclerView.ViewHolder {
        private TextView tvPlaneName, tvPlaneCategory, tvLocation;

        private PlaneViewHolder(@NonNull View itemView) {
            super(itemView);
            tvPlaneName = itemView.findViewById(R.id.tv_plane_name);
            tvPlaneCategory = itemView.findViewById(R.id.tv_plane_category);
            tvLocation = itemView.findViewById(R.id.tv_location);
        }
    }
}
