package pl.jsystems.android.samoloty.db;

import android.arch.persistence.room.TypeConverter;

import pl.jsystems.android.samoloty.PlaneCategory;

public class PlaneConverters {
    @TypeConverter
    public static String fromCategory(PlaneCategory category) {
        return category != null ? category.name() : null;
    }

    @TypeConverter
    public static PlaneCategory toCategory(String categoryString) {
        return categoryString != null
                ? PlaneCategory.valueOf(categoryString)
                : null;
    }
}
