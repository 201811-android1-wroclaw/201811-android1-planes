package pl.jsystems.android.samoloty;

public enum PlaneCategory {
    PASSENGER, MILITARY, TRANSPORT, PERSONAL
}
