package pl.jsystems.android.samoloty;

import java.util.List;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        new DisplayPlanesAsyncTask().execute();
    }

    class DisplayPlanesAsyncTask extends AsyncTask<Void, Void, List<Plane>> {
        @Override
        protected List<Plane> doInBackground(Void... voids) {
            return PlaneApplication.getDb().planeDao().findAll();
        }

        @Override
        protected void onPostExecute(List<Plane> planes) {
            super.onPostExecute(planes);
            LatLngBounds bounds = null;
            for (Plane plane : planes) {
                if (plane.getLastLocationTime() == null) {
                    continue;
                }
                LatLng position = new LatLng(plane.getLatitude(),
                        plane.getLongitude());
                mMap.addMarker(new MarkerOptions()
                        .position(position)
                        .title(plane.getName()));

                if (bounds == null) {
                    bounds = new LatLngBounds(position, position);
                } else {
                    bounds = bounds.including(position);
                }
            }
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 0));
        }
    }
}
