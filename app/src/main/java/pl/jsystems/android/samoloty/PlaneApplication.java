package pl.jsystems.android.samoloty;

import android.app.Application;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.migration.Migration;
import android.content.Intent;
import android.support.annotation.NonNull;

import pl.jsystems.android.samoloty.db.PlaneDatabase;

public class PlaneApplication extends Application {
    private static PlaneDatabase planeDatabase;

    public static PlaneDatabase getDb() {
        return planeDatabase;
    }

    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE plane ADD COLUMN latitude DOUBLE");
            database.execSQL("ALTER TABLE plane ADD COLUMN longitude DOUBLE");
            database.execSQL("ALTER TABLE plane ADD COLUMN altitude DOUBLE");
        }
    };

    private static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE plane ADD COLUMN lastLocationTime INTEGER");
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        planeDatabase = Room.databaseBuilder(this,
                PlaneDatabase.class, "planes")
                .addMigrations(MIGRATION_1_2, MIGRATION_2_3)
                .build();

        startService(new Intent(this, PlaneLocationUpdateService.class));
    }
}
