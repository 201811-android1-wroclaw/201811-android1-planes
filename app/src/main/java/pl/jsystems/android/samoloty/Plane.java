package pl.jsystems.android.samoloty;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Plane {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private int frequency;
    private PlaneCategory category;
    private boolean enabled = true;

    private Double latitude;
    private Double longitude;
    private Double altitude;
    private Long lastLocationTime;

    public Plane(int id, String name, int frequency, PlaneCategory category) {
        this.id = id;
        this.name = name;
        this.frequency = frequency;
        this.category = category;
    }

    public Plane() {

    }

    public Long getLastLocationTime() {
        return lastLocationTime;
    }

    public void setLastLocationTime(Long lastLocationTime) {
        this.lastLocationTime = lastLocationTime;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public PlaneCategory getCategory() {
        return category;
    }

    public void setCategory(PlaneCategory category) {
        this.category = category;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
