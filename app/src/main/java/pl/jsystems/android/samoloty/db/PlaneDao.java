package pl.jsystems.android.samoloty.db;

import java.util.List;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import pl.jsystems.android.samoloty.Plane;

@Dao
public interface PlaneDao {
    @Insert
    void createPlane(Plane plane);

    @Query("SELECT * FROM plane")
    List<Plane> findAll();

    @Update
    void updatePlanes(List<Plane> planes);
}
