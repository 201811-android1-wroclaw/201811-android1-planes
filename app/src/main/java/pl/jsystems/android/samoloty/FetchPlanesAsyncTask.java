package pl.jsystems.android.samoloty;

import java.util.List;

import android.os.AsyncTask;

public class FetchPlanesAsyncTask extends AsyncTask<Void, Void, List<Plane>> {
    private PlaneAdapter adapter;

    public FetchPlanesAsyncTask(PlaneAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    protected List<Plane> doInBackground(Void... voids) {
        return PlaneApplication.getDb().planeDao().findAll();
    }

    @Override
    protected void onPostExecute(List<Plane> planes) {
        super.onPostExecute(planes);
        adapter.setPlanes(planes);
        adapter.notifyDataSetChanged();
    }
}
