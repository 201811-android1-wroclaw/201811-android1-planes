package pl.jsystems.android.samoloty;

import java.util.List;
import java.util.Random;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;

public class PlaneLocationUpdateService extends IntentService {
    public static final String ACTION_LOCATION_UPDATE =
            "jsystems.LOCATION_UPDATE";

    public PlaneLocationUpdateService() {
        super("PlaneLocationUpdateService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        List<Plane> planeList = PlaneApplication.getDb().planeDao().findAll();
        Random random = new Random();

        for (Plane plane : planeList) {
            if (plane.getLastLocationTime() != null && System.currentTimeMillis() - plane.getLastLocationTime() < plane.getFrequency() * 1000) {
                continue;
            }
            plane.setLatitude(-90.0 + (180.0 * random.nextDouble()));
            plane.setLongitude(-180.0 + (360.0 * random.nextDouble()));
            plane.setAltitude(12_000.0 * random.nextDouble());
            plane.setLastLocationTime(System.currentTimeMillis());
        }

        PlaneApplication.getDb().planeDao().updatePlanes(planeList);

        Intent updateBroadcast = new Intent(ACTION_LOCATION_UPDATE);
        sendBroadcast(updateBroadcast);

        scheduleNextRun();
    }

    private void scheduleNextRun() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        PendingIntent action = PendingIntent.getService(
                this,
                1024,
                new Intent(this, PlaneLocationUpdateService.class),
                PendingIntent.FLAG_UPDATE_CURRENT
        );

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 5 * 1000, action);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 5 * 1000, action);
        }
    }
}
