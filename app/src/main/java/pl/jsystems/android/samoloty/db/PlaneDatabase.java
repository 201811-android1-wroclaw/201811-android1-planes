package pl.jsystems.android.samoloty.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import pl.jsystems.android.samoloty.Plane;

@Database(version = 3, entities = {Plane.class})
@TypeConverters(PlaneConverters.class)
public abstract class PlaneDatabase extends RoomDatabase {
    public abstract PlaneDao planeDao();
}
