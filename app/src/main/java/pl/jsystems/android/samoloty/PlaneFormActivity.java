package pl.jsystems.android.samoloty;

import java.util.HashMap;
import java.util.Map;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.SeekBar;

public class PlaneFormActivity extends AppCompatActivity {
    private static final Map<Integer, PlaneCategory> CATEGORY_MAP = new HashMap<>();

    static {
        CATEGORY_MAP.put(R.id.rb_passenger_plane, PlaneCategory.PASSENGER);
        CATEGORY_MAP.put(R.id.rb_military_plane, PlaneCategory.MILITARY);
        CATEGORY_MAP.put(R.id.rb_personal_plane, PlaneCategory.PERSONAL);
        CATEGORY_MAP.put(R.id.rb_transport_plane, PlaneCategory.TRANSPORT);
    }

    private EditText etPlaneName;
    private SeekBar sbFrequency;
    private RadioGroup rgCategory;
    private CheckBox cbEnabled;
    private Button btnSavePlane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plane_form);

        etPlaneName = findViewById(R.id.et_plane_name);
        sbFrequency = findViewById(R.id.sb_frequency);
        rgCategory = findViewById(R.id.rg_category);
        cbEnabled = findViewById(R.id.cb_enabled);
        btnSavePlane = findViewById(R.id.btn_save_plane);

        rgCategory.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                btnSavePlane.setEnabled(checkedId != -1);
            }
        });
    }

    public void onSavePlane(View v) {
        Plane plane = new Plane();
        plane.setName(etPlaneName.getText().toString());
        plane.setEnabled(cbEnabled.isChecked());
        plane.setFrequency((sbFrequency.getProgress() + 1) * 5);
        plane.setCategory(CATEGORY_MAP.get(rgCategory.getCheckedRadioButtonId()));

        new InsertPlaneAsyncTask().execute(plane);
    }

    class InsertPlaneAsyncTask extends AsyncTask<Plane, Void, Void> {
        @Override
        protected Void doInBackground(Plane... planes) {
            PlaneApplication.getDb().planeDao().createPlane(planes[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            finish();
        }
    }
}
